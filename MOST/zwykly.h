#pragma once
#include "Switch.h"

#include <iostream>

class zwykly :
	public Switch
{
public:
	zwykly(){};
	void powerOff() override
	{
		std::cout << "wylaczam\n";
	};
	void powerOn() override
	{
		std::cout << "wlaczam\n";
	}

};

